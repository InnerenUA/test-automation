package com.presidio.automation;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.mashape.unirest.http.*;

public class SimpleTest {

    @Test
    public void exampleTest() {
        int a = 1;
        int b = 3;
        assertThat(a + b, is(4));

    }

    @Test
    public void dbTest() {

    }

    @Test
    public void requestTest() throws Exception {
        HttpResponse<String> response = Unirest.get("http://google.com").asString();
        assertEquals(201, response.getStatus());
    }
}
