package com.presidio.automation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FirstExample {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "jdbc:redshift://x.y.us-west-2.redshift.amazonaws.com:5439/dev";
    static final String DB_URL = "jdbc:redshift://rs-qa.perfcycle.support:5439/qa";

    // Database credentials
    static final String USER = "qa_auto";
    static final String PASS = "op2ieW3uphoo";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try {
            // STEP 2: Register JDBC driver
            Class.forName("com.amazon.redshift.jdbc42.Driver");

            // STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 4: Execute a query
            System.out.println("Listing system tables...");
            stmt = conn.createStatement();
            String sql;
            sql = "select * from information_schema.tables;";
            ResultSet rs = stmt.executeQuery(sql);

            // Get the data from the result set.
            while (rs.next()) {
                // Retrieve two columns.
                String catalog = rs.getString("table_catalog");
                String name = rs.getString("table_name");

                // Display values.
                System.out.print("Catalog: " + catalog);
                System.out.println(", Name: " + name);
            }
            // STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            } // nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } // end finally try
        } // end try
        System.out.println("Goodbye!");

        requestExample();

    }// end main

    public static void requestExample() {

    }
}// end FirstExample
